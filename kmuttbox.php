<?php

class Kmuttbox 
{
	private $client_id;
	private $client_secret;
	
	private $code;
	private $access_token;
	private $refresh_token;

	private $api_server;
	
	public function __construct($config)
	{
		session_start();
	
		$this->client_id = $config['client_id'];
		$this->client_secret = $config['client_secret'];
		
		$this->api_server = 'http://box.cpe.kmutt.ac.th/api/';
		
		if(isset($_GET['code']))
		{
			$this->code = $_GET['code'];
			
			$param = array(
				'grant_type' => 'authorization_code', 
				'code' => $this->code 
			);
			
			$url = $this->api_server . 'token';
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->client_id . ':' . $this->client_secret);
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($curl);
			
			$data = json_decode($response, true);
			if(isset($data['access_token']))
			{
				$_SESSION['access_token'] = $data['access_token'];
				$_SESSION['refresh_token'] = $data['refresh_token'];
			}
			
			header('Location:' . $_SERVER['PHP_SELF']);
		}
		else if(isset($_SESSION['access_token']))
		{
			$this->access_token = $_SESSION['access_token'];
			$this->refresh_token = $_SESSION['refresh_token'];
		}
	}
	
	public function getLoginUrl()
	{
		return $this->api_server . 'authorize?response_type=code&client_id=' . $this->client_id . '&state=xyz';
	}
	
	public function getUser()
	{
		$user = '';
		
		$data = json_decode($this->api('account_info', 'GET'), true);
		if(isset($data['username'])) $user = $data['username'];
		
		return $user;
	}
	
	public function getAccessToken()
	{
		return $_SESSION['access_token'];
	}
	
	public function getRefreshToken()
	{
		return $_SESSION['refresh_token'];
	}
	
	public function api($resource, $method, $param = array())
	{
		$postfields = array();
		$url = $this->api_server . $resource;
		if($resource != 'token' || $resource != 'auth')
			$url = $url . '?access_token=' . $this->access_token;
		
		$curl = curl_init();
		
		foreach($param as $key => $value){
			if($resource == 'token' || $resource == 'auth' || ($key == 'filename' && $value[0] == '@'))
			{
				$postfields[$key] = $value;
			}
			else $url = $url . '&' . $key . '=' . $value;
		}
		
		if($method == 'POST')
		{
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
		}
		
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		if($resource == 'token' || $resource == 'auth'){
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($curl, CURLOPT_USERPWD, $this->client_id . ':' . $this->client_secret);
		}
		
		return curl_exec($curl);
	}
}

?>
